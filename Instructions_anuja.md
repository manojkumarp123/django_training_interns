Instructions
=======

    sudo pip install virtualenv
    virtualenv venv --python=python3
    source venv/bin/activate
    pip install django djangorestframework django_extensions jupyter
    django-admin startproject quizapp
    cd quizapp
    pip freeze > requirements.txt
    psql -U postgres -h 127.0.0.1

(password: entrayn123)

    create database quizdb;
    \q

copy and modify postgress settings from https://docs.djangoproject.com/en/1.11/ref/settings/#databases with NAME='quizdb' and PASSWORD='entrayn123', and replace DATABASES = ... in settings.py

    pip freeze > requirements.txt
    add 'django_extensions' to INSTALLED_APPS in settings.py
    ./manage.py migrate
    execute ./manage.py shell_plus --notebook

Flake8 SublimeLinter Plugin
================

https://packagecontrol.io/installation#st2
    pip install flake8 && pip freeze > requirements.txt
    flake8 .

setup.cfg (create file with below contents in project base directory)
=====

    [flake8]
    exclude = docs,.tox,manage.py,src,migrations
    ignore = F405
    max-line-length = 160
    [isort]
    line_length = 79
    skip = .tox,docs
    default_section = THIRDPARTY
    known_first_party = accounts,aggregator,blog,contact,dashboard,djangoproject,docs,fundraising,legacy,members,releases,svntogit,tracdb
    known_standard_library = pathlib,xmlrpc
    combine_as_imports = true
    include_trailing_comma = true
    multi_line_output = 5

Assignment
=======

Assessments are a way to test users understanding of a subject and provide him a plan to study further.

The Design contains ‘questions’ model that can be authored by users who are authors. One ‘assessment-instance’ is generated for every user. The user is presented with details of the assessment along with the questions and previous responses to every question in a nested json  format. The user is able to submit/change response to any question by PATCH ‘assessment_instances/{assessment_instances_id}/question_attempts/{question_attempts_id}/

Create a script to populate assessment_instances and question_attempts.
Create an API to GET /assessment_instances/{assessment_instances_id}/ and PATCH assessment_instances/{assessment_instances_id}/question_attempts/{question_attempts_id}/

sample JSON structure for assessment_instances: http://www.jsoneditoronline.org/?id=85328c2761034fb4df09632b6d0f1588

Prerequisite
=======

Read and execute code related to models with foreign key relationships https://docs.djangoproject.com/en/1.10/intro/tutorial02/
